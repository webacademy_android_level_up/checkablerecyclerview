package com.webacademy.hometask.checkablerecyclerview.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;


public class CheckableView extends View implements Checkable{

    public interface OnCheckedChangeListener {
        void onCheckedChanged(boolean isChecked);
    }

    private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

    private boolean mChecked = false;

    private OnCheckedChangeListener mOnCheckedChangeListener;

    private Paint mTextPaint;

    private String mValue;

    private float mHalfW;

    private float mHalfH;

    private boolean isCheckable;

    private OnClickListener mExternalClickListener;

    final private OnClickListener mInternalClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isCheckable) {
                toggle();
            }
            if(mExternalClickListener!=null){
                mExternalClickListener.onClick(v);
            }
        }
    };


    public CheckableView(Context context) {
        this(context,null);
    }

    public CheckableView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public CheckableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        super.setOnClickListener(mInternalClickListener);
        mTextPaint = new Paint();
        mTextPaint.setTextSize(getResources().getDimension(android.support.v7.appcompat.R.dimen.abc_text_size_button_material));
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(mValue,mHalfW,mHalfH,mTextPaint);
    }

    public boolean isChecked() {
        return mChecked;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHalfW = w/2.0f;
        mHalfH = h/2.0f;
    }


    public void setChecked(boolean b) {
        if (b != mChecked) {
            mChecked = b;
            refreshDrawableState();
            if (mOnCheckedChangeListener != null) {
                mOnCheckedChangeListener.onCheckedChanged(mChecked);
            }
        }
    }

    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }


    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
    }

    public void setValue(String value) {
        this.mValue = value;
    }

    public void setCheckable(boolean checkable) {
        isCheckable = checkable;

    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        mExternalClickListener = listener;
    }
}
